# output "aws_public_ip" {
#     value = aws_instance.dev.public_ip
# }

output "aws_elb_public_dns" {
  value = aws_elb.web.dns_name
}